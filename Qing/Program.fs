﻿open System
open System.Text

open Microsoft.Scripting
open Microsoft.Scripting.Hosting
open Microsoft.Scripting.Hosting.Shell

open Qing.Lang
open Hosting
open Runtime

let createQingCommandExecutor (console: IConsole) =
    { new ICommandDispatcher with
        member this.Execute(compiledCode, scope) =
            match compiledCode with
            | null -> null
            | _ ->
                let result = compiledCode.Execute(scope)

                if result |> isNull |> not then
                    console.WriteLine(display result, Style.Out)

                result }

type QingCommandLine() =
    inherit CommandLine()

    override this.CreateCommandDispatcher() = createQingCommandExecutor this.Console

    override this.ErrorSink = ConsoleErrorSink(this.Console.WriteLine)

    override _.Logo = $"欢迎使用青语言--数心开物工作室{Environment.NewLine}"

    override this.ExecuteCommand(command) =
        if command.Trim() = "quit" then
            Environment.Exit(0)
        else
            this.ExecuteCommand(this.Engine.CreateScriptSourceFromString(command, SourceCodeKind.InteractiveCode))
            |> ignore


type QingConsoleOptions() =
    inherit ConsoleOptions(Introspection = false, AutoIndent = true, ColorfulConsole = true, HandleExceptions = true)

type QingConsoleHost() =
    inherit ConsoleHost()

    override this.ExecuteInternal() =
        AppDomain.CurrentDomain.AssemblyLoad.Add(fun e -> this.Runtime.LoadAssembly(e.LoadedAssembly))

        this.Runtime.LoadAssembly(typeof<Console>.Assembly)

        base.ExecuteInternal()

    override _.CreateRuntimeSetup() =
        let setup = ScriptRuntimeSetup()

        setup.LanguageSetups.Add(
            LanguageSetup(typeof<QingContext>.AssemblyQualifiedName, "欢迎使用青语言", [| "青" |], [| ".q" |])
        )

        setup

    override _.CreateCommandLine() = QingCommandLine()

    override _.Provider = typeof<QingContext>

    override _.CreateOptionsParser() = OptionsParser<QingConsoleOptions>()

    override _.CreateConsole(engine, commandLine, options) =
        let console = base.CreateConsole(engine, commandLine, options) :?> BasicConsole
        // BasicConsole的ConsoleCancelEventHandler事件默认使用Thread.Abort, 这个方法在.NET 5以上被弃用.
        console.ConsoleCancelEventHandler <-
            (fun sender e ->
                if e.SpecialKey = ConsoleSpecialKey.ControlC then
                    e.Cancel <- true
                    Environment.Exit(0))

        console

[<EntryPoint>]
let main args =
    Console.InputEncoding <- UnicodeEncoding()
    QingConsoleHost().Run(args)
