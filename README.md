# IronQing - 铁青

#### 项目介绍

青语言是一门完全基于中文语言习惯打造的编程语言，主要面向青少年、儿童和非专业人士。
铁青语言是青语言的一个替代编译器, "铁"取自IronPython, IronRuby和IronScheme的"Iron", 这意味着他是编译到Dynamic Language Runtime(DLR)而非自己实现的解释器的.

青语言主页：https://qingyuyan.cn

青语言文档：https://doc.qingyuyan.cn

青语言社区：https://forum.qingyuyan.cn

#### 开发说明

###### 开发环境

操作系统：Win10 x64

开发工具：VSCode

SDK：.Net8

F语言版本: F#8

#### 项目说明

众所周知, 我们决定打造一门完全开源的中文编程语言——青语言。

然而, 青语言存在许多问题, 例如速度极慢(约为Python的1/10), DLL扩展麻烦等. 因此, 我们参考了[SymPL](https://github.com/reflectronic/sympl)解释器, 开发出了「铁青」编译器.

#### 与标准Qing解释器的不同

由于实现原理大相径庭, IronQing与标准Qing在行为上区别甚大. 目前已知的区别包括:

##### CLI

IronQing的CLI使用了Microsoft.Scripting.Shell的API, 增加了许多选项, 并且REPL稍有增强, 表现为:

1. 表达式返回的`空`结果不被打印到控制台;
2. 需要输入多行时, 不需要手动用`~`符号注明. REPL会自动根据括号匹配情况确定是否输入完成.

##### 标准库

由于可以调用.Net类库的成员, IronQing的标准库可以由青语言自己实现.目前标准库只实现了About, Cast和Collect的部分内容.

##### 语法

IronQing的语法和Qing的略有不同:

1. 支持多种数字数字类型, 由后缀区分:

|整数|后缀|对应.Net类型|
|---|---|---|
|是|无|Int32|
|是|y|SByte|
|是|uy|Byte|
|是|s|Int16|
|是|us|UInt16|
|是|u|UInt32|
|是|l|Int64|
|是|ul|UInt64|
|否|无|Decimal|
|否|f|Double|
|否|sf|Single|

2. 赋值运算符(`:`, `=`)改为右结合, 这就意味者下面的代码

```
#x: #y: 1
```

在IronQing中是合法的, 在Qing中则不是.

3. 增加了`委托`关键字, 相当于匿名函数语法`@[...]{...}`中的`@`. 创建的委托是`System.Func`的实例. 和函数相比, 委托不支持文档, 默认参数和参数省略, 但性能显著优于函数.

4. 不在运行时区分函数(`@...`)和非函数(`#...`).

5. 增加了`导入`语句, 用来导入命名空间和类:

```
导入 #System
导入 #System#Console
```

6. 禁用了一些有歧义的语法.

##### 语义

1. 不再区分原生函数与用户定义函数.

2. 循环表达式返回`空`而不是循环的次数.

##### 语言扩展

由于与.NET类库的互操作能力, IronQing理论上不需要通过C#, F#等语言进行手动扩展. 如果有必要的话, 请注意以下与Qing原生函数机制的区别:

1. 没有`Native`类用于继承. 你可以创建委托, 用委托和参数列表实例化`Qing.Lang.Runtime.QingFunction`. 这给了"原生"函数和用户定义函数一样的关键字传参能力.

2. 没有`Expr`类, 所有的值都是合法的.NET类型. 青语言类型对应的.NET类型如下表所示:

|青语言类型|.NET类型|
|---|---|
|空|(`null`)|
|逻辑|`System.Boolean`|
|二进制|`System.Byte[]`|
|整数|`System.Int32`|
|小数|`System.Decimal`|
|字符串|`System.String`|
|数组|`System.Collections.ArrayList`|
|对象|`System.Dynamic.ExpandoObject`|
|代码块|`System.Action`|
|标签|`Qing.Lang.Runtime.QingTag`|
|函数/原生函数|`Qing.Lang.Runtime.QingFunction`|
|语法类型, `Tp`字段为`Var`, `Op`等|`Qing.Lang.QingExpression.QingExpressionData`的UnionCase|

后三个类型不在BCL中提供. `QingFunction`前已述及. `QingTag`定义如下:

```fsharp
type QingTag =
    { Name: string
      Attrs: Dictionary<string, obj>
      Children: ArrayList }
```

`QingExpressionData`是一个F#可区分联合类型, 一般不常用.

#### 性能

##### 测试环境

Intel Xeno E5-2630 @ 2.4GHz, 64GB 内存

##### 测试代码

Qing:

```
@fib = @[#x] {
    如果 #x < 2 { #x }
    否则 { @fib[#x - 1] + @fib[#x - 2] }
}
@计时[{@fib[35]}]
```

IronQing(局部变量):

```
导入 #System
@test = @[] {
    @fib: @[#x] {
        如果 #x < 2 { #x }
        否则 { @fib[#x - 1] + @fib[#x - 2] }
    }
    @fib[35]
}
#watch = #System#Diagnostics#Stopwatch[]
#watch@Start[]
@test[]
#watch@Stop[]
#watch#ElapsedMilliseconds
```

IronQing(全局变量):

```
@fib: 委托[#x] {
    如果 #x < 2 { #x }
    否则 { @fib[#x - 1] + @fib[#x - 2] }
}
#watch = #System#Diagnostics#Stopwatch[]
#watch@Start[]
@fib[35]
#watch@Stop[]
#watch#ElapsedMilliseconds
```

##### 测试命令

Qing:

QIDE"运行"按钮

IronQing:

```
dotnet run -c Release <filename>
```

##### 测试结果

Qing:

37.0451818

IronQing(局部变量):

10506

换用委托后, 结果为: 1562

IronQing(全局变量):

10193

换用委托后, 结果为: 2198

##### 结论

IronQing的性能显著优于Qing, 尤其是使用委托时.

使用时可能会发现IronQing要很久才能给出结果, 这是DLR Shell启动时间长所致.

#### 注意事项

青语言没有一份精确的语言规范, 因此前文提到的语言差异必然是不全的, 请在编程的时候多加注意.

IronQing编译器严重缺乏测试, 包含了大量的Bug, 希望能在后续逐渐修复.

IronQing和.NET交互能力欠佳, 例如, 目前还不支持事件等特性. 此外, IronQing没有做足够的封装, 暴露了很多.编译器和NET运行时的细节, 没有实现青语言「简单」的设计原则.

Iron的标准库不完善, 只能依赖.NET类和方法进行编程.
