module internal Qing.Lang.Symbols

open System.Collections
open System.Reflection

open Runtime

let toString = typeof<QingOps>.GetMethod("Display")
let makeExn = typeof<exn>.GetConstructor([| typeof<string> |])

let makeArray = typeof<QingOps>.GetMethod("MakeArray")
let makeObject = typeof<QingOps>.GetMethod("MakeObject")
let makeTag = typeof<QingOps>.GetMethod("MakeTag")
let await = typeof<QingOps>.GetMethod("Await")

let equals =
    typeof<QingOps>.GetMethod("Equals", BindingFlags.Static ||| BindingFlags.Public)

let import = typeof<QingOps>.GetMethod("Import")

let makeQingFunction = typeof<QingFunction>.GetConstructors()[0]
let invokeQingFunction = typeof<QingFunction>.GetMethod("Invoke")

let getEnumerator = typeof<QingOps>.GetMethod("GetEnumerator")
let moveNext = typeof<IEnumerator>.GetMethod("MoveNext")
let current = typeof<IEnumerator>.GetProperty("Current")

let stringAdd =
    typeof<string>.GetMethod("Concat", [| typeof<string>; typeof<string> |])

let stringSub = typeof<QingOps>.GetMethod("StringSub")
let stringMul = typeof<QingOps>.GetMethod("StringMul")
let stringDiv = typeof<QingOps>.GetMethod("StringDiv")
