// DLR Shell允许多行输入, 但需要LanguageContext判断用户输入是否完成.
// 这里用FParsec实现了一个简单的递归下降解析器, 判断代码中的括号是否匹配.
module Qing.Lang.BracketMatcher

open FParsec

open Parser.Parsers
open Parser.Tokens

let parseNode, parseNodeRef = createParserForwardedToRef ()

let parseArbitaryChars = skipMany1 (noneOf ";；\"“()（）[]【】{}｛｝《》") >>% true

let parseString =
    (lquotec >>. stringcontent >>. ((rquotec >>% true) <|> (eof >>% false)))
    <|> (lquotee >>. stringcontent >>. ((rquotee >>% true) <|> (eof >>% false)))

let parseAtom = parseArbitaryChars <|> parseString .>> spaces

let parseBracketed =
    let pairs = [ lparen, rparen; lbracket, rbracket; lbrace, rbrace; ltag, rtag ]

    seq {
        for left, right in pairs do
            left >>. skipMany parseNode >>. ((right >>% true) <|> (eof >>% false))
    }
    |> choice

parseNodeRef.Value <- parseAtom <|> parseBracketed

let parseProgram =
    parse {
        do! spaces

        match! many parseNode with
        | [] -> return true
        | xs -> return List.last xs
    }

// 括号不匹配有两种情况:
//     1. 如"(foo", 这是用户输入未完成, 需要换行继续输入;
//     2. 如"(foo]", 这是用户输入错误, 不可能通过继续输入使之成为括号匹配的字符串.
// 对于情况1, isMatch返回false; 对于情况2, isMatch返回true, 以便让Parser报告错误.

let isComplete str =
    // Parser模块里的解析器有用户状态.
    match runParserOnString parseProgram [] "" str with
    | Success(false, _, _) -> false
    | _ -> true
