module Qing.Lang.Parser

open System
open System.Globalization
open System.IO
open System.Linq.Expressions

open Microsoft.Scripting

open FParsec

open QingExpression
open Util

type NumberLiteral with

    member this.Suffix =
        let chars =
            [ this.SuffixChar1; this.SuffixChar2; this.SuffixChar3; this.SuffixChar4 ]
            |> List.take this.SuffixLength

        String.Join("", chars)

type ParserError = ParserError of message: string * span: SourceSpan * errorCode: int * severity: Severity

let private positionToSourceLocation (position: Position) =
    SourceLocation(int position.Index, int position.Line, int position.Column)

let private makeSourceSpan oldPosition newPosition =
    SourceSpan(positionToSourceLocation oldPosition, positionToSourceLocation newPosition)

// 为什么这些函数都不用camelCase呢?

let private withpos parser : Parser<QingExpression, _> =
    parse {
        let! oldPosition = getPosition
        let! result = parser
        let! newPosition = getPosition

        return
            { Data = result
              SourceSpan = makeSourceSpan oldPosition newPosition }
    }

let private reportError oldPosition message severity errorCode : Parser<_, ParserError list> =
    parse {
        let! newPosition = getPosition
        let! errors = getUserState

        do!
            setUserState (
                ParserError(message, makeSourceSpan oldPosition newPosition, errorCode, severity)
                :: errors
            )

        return ()
    }

let private spaces = regex @"[\s,，]*([;；]{2}[\s\S]*[;；]{2}|[;；].*|[\s,，]*)*" >>% ()

let private spaces' =
    notFollowedBy (satisfy (fun c -> Char.IsLetterOrDigit(c) || c = '_')) .>> spaces

module internal Tokens =
    let lquotec = skipString "“"
    let lquotee = skipString "\""
    let rquotec = skipString "”"
    let rquotee = skipString "\""
    let sign (chars: string) = anyOf chars >>. spaces <?> chars[..0]
    let keyword s = skipString s >>. spaces' <?> s
    let lbracket = sign "[【"
    let rbracket = sign "]】"
    let lbrace = sign "{｛"
    let rbrace = sign "}｝"
    let lparen = sign "(（"
    let rparen = sign ")）"
    let ltag = sign "《"
    let rtag = sign "》"
    let at = sign "@"
    let dot = sign "、"

    let operator eng chn result =
        skipString eng >>. spaces <|> (regex chn >>. spaces') >>% result <?> eng

    let assign = regex ":|：" >>. spaces <|> (skipString "设为" >>. spaces') >>% ()
    let assignrec = operator "=" "为" ()
    let addeq = operator "+=" "自加|加等" ExpressionType.Add
    let subeq = operator "-=" "自减|减等" ExpressionType.Subtract
    let muleq = operator "*=" "自乘|乘等" ExpressionType.Multiply
    let diveq = operator "/=" "自除|除等" ExpressionType.Divide
    let modeq = operator "%=" "自模|模等" ExpressionType.Modulo
    let poweq = stringReturn "**=" ExpressionType.Power .>> spaces
    let add = operator "+" "加" ExpressionType.Add
    let sub = operator "-" "减" ExpressionType.Subtract
    let mul = operator "*" "乘" ExpressionType.Multiply
    let div = operator "/" "除" ExpressionType.Divide
    let mod' = operator "%" "模" ExpressionType.Modulo
    let pow = stringReturn "**" ExpressionType.Power .>> spaces
    let eq = operator "==" "等于" ExpressionType.Equal

    let ne =
        ([ "!="; "<>" ] |> List.map skipString |> choice >>. spaces)
        <|> (skipString "不等于" >>. spaces')
        >>% ExpressionType.NotEqual
        <?> "<>"

    let lt = operator "<" "小于" ExpressionType.LessThan
    let gt = operator ">" "大于" ExpressionType.GreaterThan
    let le = operator "<=" "小于等于" ExpressionType.LessThanOrEqual
    let ge = operator ">=" "大于等于" ExpressionType.GreaterThanOrEqual
    let and' = operator "&&" "且" ExpressionType.AndAlso
    let or' = operator "||" "或" ExpressionType.OrElse
    let not' = keyword "取反"
    let async = keyword "异步"
    let if' = keyword "如果"
    let elif' = keyword "再则"
    let else' = keyword "否则"
    let while' = keyword "当"
    let do' = keyword "执行"
    let until = keyword "直到"
    let repeat = keyword "重复"
    let foreach = keyword "遍历"
    let try' = keyword "尝试"
    let catch = keyword "排查"
    let finally' = keyword "例行"
    let throw = keyword "抛出"
    let return' = keyword "返回"
    let break' = keyword "跳出"
    let continue' = keyword "继续"
    let switch = keyword "匹配"
    let default' = keyword "默认"
    let await = keyword "等待"
    let import = keyword "导入"
    let delegate' = keyword "委托"
    let meta = keyword "元"

open Tokens

module Parsers =
    let pexprnolabel, pexprRef = createParserForwardedToRef ()

    let pexpr = pexprnolabel <?> "表达式"

    let pblock = lbrace >>. many pexpr .>> rbrace .>> spaces

    let stringcontent =
        let unescapedChar = noneOf "\\\"”"

        let escapedChar =
            [ ("\\\"", '"')
              ("\\”", '”')
              ("\\\\", '\\')
              ("\\/", '/')
              ("\\b", '\b')
              ("\\f", '\f')
              ("\\n", '\n')
              ("\\r", '\r')
              ("\\t", '\t') ]
            |> List.map (fun (toMatch, result) -> stringReturn toMatch result)
            |> choice

        let unicodeChar =
            parse {
                let! m = regex @"\\u(\d{4})"
                return Int32.Parse(m[1..], NumberStyles.HexNumber) |> char
            }

        let onechar = choice [ unescapedChar; escapedChar; unicodeChar ]

        manyChars onechar

    let quotedstring =
        stringcontent |> between lquotec rquotec
        <|> (stringcontent |> between lquotee rquotee)
        .>> spaces

    let pconstant =
        let pnumber =
            parse {
                let! oldPosition = getPosition

                let tryConvert convert (s: string) =
                    match convert s with
                    | true, result -> result |> box |> preturn
                    | false, _ -> reportError oldPosition $"无效的数字字面量\"{s}\"" Severity.Error 1 >>. preturn null

                let converts =
                    [ (true, ""), tryConvert Int32.TryParse
                      (true, "y"), tryConvert SByte.TryParse
                      (true, "uy"), tryConvert Byte.TryParse
                      (true, "s"), tryConvert Int16.TryParse
                      (true, "us"), tryConvert UInt16.TryParse
                      (true, "u"), tryConvert UInt32.TryParse
                      (true, "l"), tryConvert Int64.TryParse
                      (true, "ul"), tryConvert UInt64.TryParse
                      (false, ""), tryConvert Decimal.TryParse
                      (false, "f"), tryConvert Double.TryParse
                      (false, "sf"), tryConvert Single.TryParse ]
                    |> Map

                let! number =
                    numberLiteral
                        (NumberLiteralOptions.AllowSuffix
                         ||| NumberLiteralOptions.AllowMinusSign
                         ||| NumberLiteralOptions.AllowFraction)
                        "数字"

                do! spaces'

                let suffix = number.Suffix

                match Map.tryFind (number.IsInteger, suffix) converts with
                | Some convert -> return! convert number.String
                | None ->
                    do! reportError oldPosition $"无法识别的数字后缀\"{suffix}\"" Severity.Error 2
                    return null
            }

        let pbool =
            (stringReturn "真" (box true)) <|> (stringReturn "假" (box false)) .>> spaces'

        let pnil = stringReturn "空" null .>> spaces'

        let pbinary =
            regex @"0[xX]([\da-fA-F]+)" .>> spaces'
            |>> (stringSkip 2 >> hexStringToBytes >> box)

        let pstring = quotedstring |>> box

        [ pbinary; pnumber; pbool; pnil; pstring ] |> choice |>> Constant <?> "常量"

    let pvar' = regex @"#([\w`]+)" |>> stringSkip 1
    let pfunc' = regex @"@([\w`]+)" |>> stringSkip 1

    let pvar = pvar' |>> Var
    let pfunc = pfunc' |>> Func
    let pvarorfunc = pvar <|> pfunc <?> "变量名(#...或@...)"

    let pargs =
        let rec loop posargs kwargs =
            parse {
                let! oldPosition = getPosition

                match! opt (pvarorfunc .>> spaces .>> assignrec .>> notFollowedBy (pchar '=') |> attempt) with
                | Some key when kwargs |> Seq.map fst |> Seq.contains key ->
                    do! reportError oldPosition "重复的关键字参数" Severity.Error 3
                    return! loop posargs kwargs
                | Some key ->
                    let! value = pexpr
                    return! loop posargs ((key, value) :: kwargs)
                | None ->
                    match! opt pexpr with
                    | Some _ when kwargs |> List.isEmpty |> not ->
                        do! reportError oldPosition "位置参数必须在关键字参数之后" Severity.Error 4
                        return! loop posargs kwargs
                    | Some posarg -> return! loop (posarg :: posargs) kwargs
                    | None -> return QingArgs(List.rev posargs, List.rev kwargs)
            }

        loop [] []

    let ppath =
        let pseg =
            let varOrInt (s: string) =
                match System.Int32.TryParse(s) with
                | true, i -> IntSegment i
                | false, _ -> VarSegment s

            // 路径中的右方括号不能接受尾随空白.
            let rbracket' = anyOf "]］" <?> "]"

            let pfuncorcallmember =
                pfunc' .>>. opt (spaces >>. lbracket >>. pargs .>> rbracket' |> attempt)
                |>> (function
                | memberName, Some args -> CallMemberSegment(memberName, args)
                | func, None -> FuncSegment func)

            [ pvar' |>> varOrInt
              pfuncorcallmember
              skipString "#" >>. lbracket >>. pexpr .>> rbracket' |>> ExprSegment
              lbracket >>. pargs |>> CallSegment .>> rbracket' ]
            |> choice

        let psegs =
            many pseg .>>. opt (dot >>. pexpr)
            |>> (function
            | segs, None -> segs
            | init, Some last -> init @ [ CallSegment(QingArgs([ last ], [])) ])

        pvarorfunc .>>. psegs .>> spaces' |>> QingPath <?> "路径"

    let pcodeblock = pblock |>> CodeBlock

    let parray = lbracket >>. spaces >>. many pexpr .>> rbracket |>> Array <?> "数组"

    let pobject =
        let rec loop entries =
            parse {
                let! oldPosition = getPosition

                match! opt (pvarorfunc .>> spaces .>> assign) with
                | Some key when entries |> Seq.map fst |> Seq.contains key ->
                    do! reportError oldPosition "重复的对象键" Severity.Error 5
                    return! loop entries
                | Some key ->
                    let! value = pexpr
                    return! loop ((key, value) :: entries)
                | None -> return Object(List.rev entries)
            }

        attempt (at >>. lbrace) >>. loop [] .>> rbrace <?> "对象"

    let plambda =
        let rec loop parameters defparams =
            parse {
                let! oldPosition = getPosition

                match! opt (pvarorfunc .>> spaces) with
                | Some key when defparams |> Seq.map fst |> Seq.append parameters |> Seq.contains key ->
                    do! reportError oldPosition "重复的参数" Severity.Error 6
                    return! loop parameters defparams
                | Some key ->
                    match! opt (assignrec >>. pexpr) with
                    | Some def -> return! loop parameters ((key, def) :: defparams)
                    | None when defparams |> List.isEmpty |> not ->
                        do! reportError oldPosition "位置参数必须在关键字参数之后" Severity.Error 7
                        return! loop parameters defparams
                    | None -> return! loop (key :: parameters) defparams
                | None ->
                    let! desc = opt quotedstring
                    return List.rev parameters, List.rev defparams, desc
            }


        parse {
            let! isAsync = attempt (at >>. opt async |>> _.IsSome .>> lbracket)
            let! parameters, defparams, desc = loop [] []
            let! body = rbracket >>. pblock
            return Lambda(isAsync, parameters, defparams, desc, body)
        }

    let pdelegate =
        let rec pparams acc =
            parse {
                let! oldPosition = getPosition

                match! opt (pvarorfunc .>> spaces) with
                | Some param when acc |> Seq.contains param ->
                    do! reportError oldPosition "重复的参数" Severity.Error 6
                    return! pparams acc
                | Some param -> return! pparams (param :: acc)
                | None -> return List.rev acc
            }

        delegate' >>. lbracket >>. pparams [] .>> rbracket .>>. pblock |>> Delegate

    let ptag =
        let rec loop acc =
            parse {
                let! oldPosition = getPosition

                match! opt (pvar' .>> spaces .>> assignrec) with
                | Some key when acc |> Seq.map fst |> Seq.contains key ->
                    do! reportError oldPosition "重复的标签属性" Severity.Error 8
                    return acc, []
                | Some key ->
                    let! value = pexpr
                    return! loop ((key, value) :: acc)
                | None ->
                    let! children =
                        opt (spaces >>. (many pexpr |> between lbracket rbracket))
                        |>> Option.defaultValue []

                    return List.rev acc, children
            }

        ltag >>. regex @"\w+" .>> spaces .>>. loop [] .>> rtag
        |>> (fun (name, (attrs, children)) -> Tag(name, attrs, children))
        <?> "标签"

    let pparen = lparen >>. pexpr .>> rparen

    let pimport = import >>. many pvar' .>> spaces' |>> Import

    let ptarget =
        parse {
            let! oldPosition = getPosition

            match! ppath with
            | QingPath(_, []) as target -> return target
            | QingPath(_, subsequence) as path when
                (match List.last subsequence with
                 | CallSegment _
                 | CallMemberSegment(_, _) -> true
                 | _ -> false)
                ->
                do! reportError oldPosition "不能对函数调用赋值" Severity.Error 9
                return path
            | target -> return target
        }

    let passign =
        let augops = [ addeq; subeq; muleq; diveq; modeq; poweq ] |> choice

        parse {
            let! target = ptarget

            match! opt augops with
            | Some augop ->
                let! value = pexpr
                return AugAssign(target, augop, value)
            | None ->
                let! recursive = assign >>% false <|> (assignrec >>% true)
                let! value = pexpr
                return Assign(recursive, target, value)
        }
        |> attempt

    let pif =
        parse {
            let! test = if' >>. pexpr
            let! body = pblock

            let! elifs =
                many
                <| parse {
                    let! oldPosition = getPosition
                    let! test = elif' >>. pexpr
                    let! body = pblock
                    let! newPosition = getPosition
                    return makeSourceSpan oldPosition newPosition, test, body
                }

            let! orelse = opt (else' >>. pblock) |>> Option.defaultValue []

            let orelse =
                let folder orelse (span, test, body) =
                    [ { Data = If(test, body, orelse)
                        SourceSpan = span } ]

                List.fold folder orelse elifs

            return If(test, body, orelse)
        }

    let pfororwhile =
        parse {
            let! testorinit = while' >>. pexpr
            // "{...}"既可以是CodeBlock字面量, 又可以是语法结构的组成部分.
            match! pexpr with
            | { Data = CodeBlock(body) } ->
                let test = testorinit
                return While(test, body)
            | test ->
                let init = testorinit
                let! step = pexpr
                let! body = pblock
                return For(init, test, step, body)
        }

    let puntil = do' >>. pblock .>> until .>>. pexpr |>> (swap >> Until)

    let prepeat = repeat >>. pexpr .>>. pblock |>> Repeat

    let pforeach =
        parse {
            do! foreach

            match! pexpr with
            | { Data = Assign(true, iter, { Data = Path(QingPath(target, [])) }) } ->
                let! body = pblock

                return
                    ForEach(
                        target,
                        { Data = Path iter
                          SourceSpan = SourceSpan.None },
                        body
                    )
            | iter ->
                do! assignrec
                let! target = pvarorfunc .>> spaces
                let! body = pblock
                return ForEach(target, iter, body)
        }

    let ptry =
        let f (((body, target), handlerbody), finalbody) =
            Try(body, QingHandler(target, handlerbody), finalbody |> Option.defaultValue [])

        try' >>. pblock .>> catch .>>. pvar' .>> spaces
        .>>. pblock
        .>>. opt (finally' >>. pblock)
        |>> f

    let pthrow = throw >>. pexpr |>> Throw
    let preturn' = return' >>. pexpr |>> Return
    let pbreak = break' >>% Break
    let pcontinue = continue' >>% Continue
    let pawait = await >>. pexpr |>> Await

    let pquote =
        parse {
            do! meta
            let! { Data = expr } = pexpr

            match expr with
            | Constant _ -> return expr
            | _ -> return Constant expr
        }

    let pswitch =
        let rec loop cases =
            parse {
                match! opt pexpr with
                | Some value ->
                    let! body = pblock
                    return! loop (QingCase(value, body) :: cases)
                | None ->
                    let! def = opt (default' >>. pblock)
                    return List.rev cases, def |> Option.defaultValue []
            }

        parse {
            let! subject = switch >>. pexpr .>> lbrace
            let! cases, def = loop [] .>> rbrace
            return Switch(subject, cases, def)
        }

    let patom =
        [ ppath |>> Path; pconstant; parray; ptag; pobject; pcodeblock ]
        |> choice
        |> withpos
        <|> pparen

    let pbinop =
        let pfactor = patom <|> (not' >>. patom |>> Not |> withpos)

        let binop operand ops =
            let maybeOpAndRight = choice ops .>>. operand |> opt

            let rec loop acc =
                parse {
                    let! oldPosition = getPosition

                    match! maybeOpAndRight with
                    | Some(op, right) ->
                        let! newPosition = getPosition
                        let sourceSpan = makeSourceSpan oldPosition newPosition

                        return!
                            loop
                                { Data = BinOp(acc, op, right)
                                  SourceSpan = sourceSpan }
                    | None -> return acc
                }

            operand >>= loop

        let ppower = binop pfactor [ pow ]
        let pterm = binop ppower [ mul; div; mod' ]
        let parith = binop pterm [ add; sub ]
        let pcomp = binop parith [ eq; ne; le; ge; lt; gt ]
        let pand = binop pcomp [ and' ]

        binop pand [ or' ]

    pexprRef.Value <-
        [ passign
          plambda
          pdelegate
          pif
          pfororwhile
          puntil
          prepeat
          pforeach
          ptry
          pthrow
          preturn'
          pbreak
          pcontinue
          pswitch
          pawait
          pimport
          pquote ]
        |> choice
        |> withpos
        <|> pbinop

    let pmodule =
        let rec loop acc =
            parse {
                match! opt eof with
                | Some() -> return List.rev acc
                | None ->
                    let! ast = pexpr
                    return! loop (ast :: acc)
            }

        spaces >>. loop []

let private formatFParsecError (error: FParsec.Error.ParserError) =
    // DLR Shell会自动显示源代码位置, 不需要FParsec的默认错误格式化.
    let dummyPositionPrinter _ _ _ _ = ()
    use sw = new StringWriter()
    error.WriteTo(sw, dummyPositionPrinter)
    string sw

let parse source sourceUnit (errors: ErrorSink) =
    let addErrorsToSink parserErrors =
        for (ParserError(message, span, errorCode, severity)) in List.rev parserErrors do
            errors.Add(sourceUnit, message, span, errorCode, severity)

    match runParserOnString Parsers.pmodule [] "" source with
    | Success(result, parserErrors, _) ->
        addErrorsToSink parserErrors
        result
    | Failure(_, error, parserErrors) ->
        addErrorsToSink parserErrors
        let position = error.Position

        errors.Add(
            sourceUnit,
            formatFParsecError error,
            SourceSpan(
                positionToSourceLocation position,
                SourceLocation(int position.Index + 1, int position.Line, int position.Column + 1)
            ),
            100,
            Severity.FatalError
        )

        []
